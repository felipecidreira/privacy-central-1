# Dogma Data Privacy

## Central da Privacidade
A Central da Privacidade oferece acesso, por parte dos titulares de dados, às principais funcionalidades da Dogma:

1. Gestão de Cookies
2. Gestão de Consentimentos
3. Pedidos (acesso, remoção e retificação de dados)

### Inserção no Site
É fácil inserir a Central da Privacidade no seu site. Idealmente, a Central da Privacidade deve estar presente em todas as páginas.  

Para fazer a inserção no site é necessário executar 2 passos:

1. Registro do CNAME
2. Inserção do código javascript em todas as páginas que devem mostrar a Central da Privacidade ou interagir com o objeto _window.dogma_ via javascript

#### Registro do CNAME
A internet funciona com endereços IP. Na versão 4, os endereços IP são do tipo `192.168.0.1`.  
Para acessar qualquer site na internet é necessário saber o endereço IP do servidor que hospeda o site que você gostaria de acessar, mas seria muito difícil saber de cabeça os endereços IP de todos os sites que nós acessamos.  

Por este motivo, todos os sites da internet são obrigado a utilizar o serviço de _DNS_. O _DNS_ é um serviço que transforma nomes de sites em endereços IP. É só por causa do _DNS_ que nós podemos acessar o _google.com_ sem saber o IP dele.

Para que a integração entre a Central da Privacidade e o seu site funcione, é necessário fazer uma alteração no _DNS_ do seu site, inserindo um registro do tipo _CNAME_. 
Por exemplo, se o seu site for nebers.com.br, o registro de _CNAME_ deve ser **dogma**.neber.com.br -> **api.dogma.legal**

O [painel de controle](https://console.dogmal.legal) indica em detalhes como fazer a configuração.

#### Inserção do código no site
Além das configurações de _DNS_ descritas acima, é necessário inserir o código javascript gerado pela Dogma para a integração entre a Central da Privacidade e o seu site. Para fazer isso basta acessar o [painel de controle](https://console.dogmal.legal), copiar o código para o seu site, e colar o código em todas as páginas do seu site que devem mostrar a Central da Privacidade ou interagir com o objeto _window.dogma_ via javascript

### Customização
É possível customizar a Central da Privacidade para as suas necessidades.  
As principais formas de customização são:

1. Temas
2. Variante
3. Estilo

#### Temas
Existem 3 temas de cores disponíveis: _blue_, _light_ e _dark_.
Para escolher um dos temas modifique a propriedade _theme_ do objeto _options_ que se encontra no código gerado:

```javascript
var options = {
    // ...
    theme: 'blue',
    // ...
};
```

#### Variante
Existem 2 variações do banner: _classic_ e _floating_.  
Para escolher uma das variações modifique a propriedade _variant_ do objeto _options_ que se encontra no código gerado:

```javascript
var options = {
    // ...
    variant: 'floating',
    // ...
};
```

#### Estilo
Além das opções acima, é possível configurar outros estilos do banner usando _css_ diretamente. Para isso funcionar crie um tag `<style>` antes do código do banner e aplique as mudanças desejadas. Por exemplo, para alterar a cor dos botões:

```html
<style>
    .dgbn button.action {
        background: green !important;
        color: yellow !important;
    }
</style>

<script>/* código gerado pela Dogma para o seu site */</script>
```

### API Javascript
É possível interagir com a Central da Privacidade via javascript em qualquer página que tenha o código da Central da Privacidade instalado.

Todas as páginas com o código da Central da Privacidade exportam a API Javascript da dogma através do objeto 'dogma' no escopo global da janela.  

```javascript
window.dogma
```

As principais funcionalidades disponíveis são as seguintes:

1. Cookies
2. Consentimentos
3. Pedidos
4. Banner

#### Cookies
```javascript
const cookies = window.dogma.cookies()
```

O objeto _cookies_ tem os seguintes métodos:  

1. _list()_ para listar os grupos e cookies existentes
2. _accept()_ para aceitar ou recusar um cookie e/ou grupo de cookies
3. _onChange()_ chamada de _callback_ quando um cookie e/ou grupo é aceito ou recusado

##### Listando os Cookies existentes
Para listar os cookies cadastrados na plataforma use o método _list()_ do objeto cookies. 

```javascript 
await window.dogma.cookies().list().then(r => r.json())
```

A chamada retorna a lista de grupos e cookies em uma _Promise_. Exemplo:
```javascript
[
    {
        "id": 1,
        "name": "Nome do Grupo",
        "description": "Descrição do Grupo",
        "required": true, // indica se todos os cookies deste grupo são obrigatórios
        "preferences":[{   
            "id"        :1,
            "accepted"  :false,
            "name"      :"nome_do_cookie",
            "provider"  :"dominio.do.cookie.com.br",
            "kind"      :"http",
            "duration"  :"unlimited",
            "purpose"   :"descrição do propósito"
        }]
    }
]
```

#####  Aceitando ou recusando Grupos de Cookies ou Cookies individualmente
Para aceitar ou recusar listar os cookies cadastrados na plataforma use o método accept(accepted, group, cookie)_ do objeto cookies. 

Parâmetros:  

 *  _accepted (**boolean**): indica se o grupo/cookie foi aceito ou recusado
 *  _group_  (**number**): o id do grupo
 *  _cookie_ (**number**): o id do cookie. Este parâmetro é opcional. Caso seja omitido o valor de _accepted_ será aplicado à todos os cookies do grupo

Exemplos:  

Para aceitar todos os cookies do grupo 1
```javascript 
window.dogma.cookies().accept(true, 1)
```

Para recusar apenas o cookie 99 do grupo 42
```javascript 
window.dogma.cookies().accept(false, 42, 99)
```

**IMPORTANTE**
```  
    O aceite ou recusa de um de um grupo e/ou cookie  não implica na remoção e futura não utilização 
    do cookie por parte do site, uma vez que alguns cookies não podem ser removidos via javascript.

    Leia a seção "Recebendo Notificações de aceite ou recusa de Grupos e Cookies" para mais informações.
```

##### Recebendo Notificações de aceite ou recusa de Grupos e Cookies
Registre uma função de _callback_  no método _onChange(callback)_ para ser notificado quando um titular de dados aceita ou recusa a utilização de um grupo e/ou cookie.
```javascript 

funtion myHandler(evt) {
    console.log(`Evento: ${evt.event}`)
    console.log(`Sucesso: ${evt.success}`)
    console.log(`Resposta: ${evt.response}`)
    console.log(`Dados: ${evt.data}`)
    console.log(`Erro: ${evt.error}`)
}

window.dogma.cookies().onChange(myHandler)
```

Esta função deve ser utilizada pelo site para efetivamente remover os cookies que foram recusados ou criar os cookies que foram aceitos.


#### Consentimentos
```javascript
const consent = window.dogma.consent()
```

O objeto _consent_ tem os seguintes métodos:  

1. _list()_ para listar as atividades de tratamento cuja base legal é o consentimento
2. _accept()_ para aceitar ou revogar uma atividade de tratamento
3. _onChange()_ chamada de _callback_ quando uma atividade é aceita ou revogada

#### Pedidos
```javascript
const inquiries = window.dogma.inquiries()
```

O objeto _inquiries_ tem os seguintes métodos:  

1. _list()_


#### Banner
```javascript
const banner = window.dogma.banner()
```

O objeto _banner_ tem os seguintes métodos:  

1. _isClosed()_ retorna _true_ quando o o banner está fechado e _false_ ou _indefined_ quando o banner está aberto 
2. _open()_ para abrir/mostrar o banner
3. _close()_ para fechar/ocultar o banner

```javascript
const banner = window.dogma.banner()

banner.close()
banner.isClosed() //retorna true
banner.open()
banner.isClosed() //retorna false

```


